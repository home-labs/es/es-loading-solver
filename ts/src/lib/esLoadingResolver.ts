import { CallerFileTrackTrace } from '@cyberjs.on/file-forklift';

import NodePath from 'path';
import url from 'url';
import fs from 'fs';

import { AbstractEsLoadingResolver } from './abstractEsLoadingResolver.js';
import { EsLoadingOptionsInterface } from './esLoadingOptionsInterface.js';


export class EsLoadingResolver extends AbstractEsLoadingResolver {

    private callerFileTrackTrace: CallerFileTrackTrace;

    private absolutePath!: string;

    constructor(fileExtension?: string, options?: EsLoadingOptionsInterface) {
        super(fileExtension, options);

        this.callerFileTrackTrace = new CallerFileTrackTrace();
    }

    protected getAbsolutePath(): string {
        return this.absolutePath;
    }

    protected resolvePath(relativePath: string): string {

        const parentDirectoryPattern = /^(\.\.\/)+/;

        const currentPath: string = NodePath.dirname(url.fileURLToPath(import.meta.url));

        // console.log(currentPath)

        // the AbstractEsLoadingResolver is who will actually import, but "import.meta.url" returns the current file url. The .removeFloors method should be used in cases where the calling file of this file is in one or more of the above directories, so use 1 or more as last parameter.
        // instalar no-dir, se for o caso, e usar o método removeChildFloorsFrom de Helper
        const relativeRootDirectory: string = this
            .removeFloors(NodePath.relative(currentPath, process.env.PWD as string), 0);

        // console.log(relativeRootDirectory)

        const fileCallerURL: string = this.callerFileTrackTrace.getFileCallerURL();

        // console.log(fileCallerURL);

        let fileCallerPath: string = NodePath.dirname(url.fileURLToPath(fileCallerURL));

        let parentFoldersCount: number;

        let absolutePath4Test: string;

        let relativeFileCallerDirectory: string;

        let relativeDirectory: string;

        let absoluteDirectory: string;

        relativePath = this
            .removeUnecessaryPathSeparator(this
                .treatPath(this.convertPathSeparator(relativePath)));

        if (parentDirectoryPattern.test(relativePath)) {

            parentFoldersCount = relativePath.split('../').length - 1;

            relativePath = relativePath.replace(parentDirectoryPattern, '');

            fileCallerPath = this.removeFloors(fileCallerPath, parentFoldersCount);
            // console.log(fileCallerPath);
        }

        relativeFileCallerDirectory = NodePath.relative(process.env.PWD as string, fileCallerPath);

        relativeDirectory = this
            .convertPathSeparator(`${relativeRootDirectory}/${relativeFileCallerDirectory}`);

        absoluteDirectory = NodePath.resolve(relativeFileCallerDirectory);

        if (this.indexPattern.test(relativePath)) {
            this.absolutePath = NodePath.normalize(NodePath
                .resolve(absoluteDirectory, `${relativePath}.${this.fileExtension}`));
            relativePath = `${relativePath}.${this.fileExtension}`;
        } else if (this.extensionPattern.test(relativePath)) {
            this.absolutePath = NodePath.normalize(NodePath
                .resolve(absoluteDirectory, `${relativePath}`));
        } else {
            absolutePath4Test = NodePath.normalize(NodePath
                .resolve(absoluteDirectory, `${relativePath}.${this.fileExtension}`));
            if (fs.existsSync(absolutePath4Test)) {
                this.absolutePath = absolutePath4Test;
            } else {
                absoluteDirectory = NodePath.resolve(absoluteDirectory, `${relativePath}`);
                this.absolutePath = NodePath.normalize(`${absoluteDirectory}/index.${this.fileExtension}`);

                relativePath = `${relativePath}/index`;
            }

            relativePath = `${relativePath}.${this.fileExtension}`;
        }

        return this
            .removeUnecessaryPathSeparator(`${relativeDirectory}/${relativePath}`);
    }

}
