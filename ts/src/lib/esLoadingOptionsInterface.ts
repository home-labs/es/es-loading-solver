export type EsLoadingOptionsInterface = {

    timeoutValue: number;

    moduleData?: {
        accessorSymbol: symbol
    };

}
